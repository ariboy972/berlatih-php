<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hasil Nilai</title>
</head>
<body>
<h1>Hasil Nilai</h1>
<?php
function tentukan_nilai($number){
    //  kode disini
    $result = "";
    if ($number >= 85 && $number <=100) {
        $result = "Sangat Baik <br>";
    }else if ($number >= 70 && $number < 85 ) {
        $result = "Baik <br>";
    }else if ($number >=60 && $number <70) {
        $result = "Cukup <br>";
    }else {
        $result = "Kurang";
    }
    return $result;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
</body>
</html>

